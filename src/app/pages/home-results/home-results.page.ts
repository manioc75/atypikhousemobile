import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController
} from '@ionic/angular';

// Modals
// import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage {
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/logement1.png';
  habitationList: any[] = [];
  value = 0;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public nav: NavController,
    public toastCtrl: ToastController,
    private route: RouterModule
  ) {
      this.http.get('https://api.myjson.com/bins/pioyh').subscribe(data => {
// process the json data
        this.habitationList = data['data'];

      });
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  openDetailHabitation(x: any) {
    this.nav.navigateForward('detail-habitation/' + x);
    console.log(x);
  }

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return popover.present();
  }

}
