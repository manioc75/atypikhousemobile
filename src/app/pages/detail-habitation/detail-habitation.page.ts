import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail-habitation',
  templateUrl: './detail-habitation.page.html',
  styleUrls: ['./detail-habitation.page.scss'],
})
export class DetailHabitationPage implements OnInit {
  descriptionHabitation: [];
  themeCover = 'assets/img/logement1.png';
  id_habit;
  constructor(

      private http: HttpClient,
      private route: ActivatedRoute
  ) {


  }

  ngOnInit() {
    this.route.params.subscribe( params => this.id_habit = params );
    console.log(this.id_habit);

    this.http.get('https://api.myjson.com/bins/pioyh').subscribe(data => {
// process the json data
      this.descriptionHabitation = data['data'];
    });
  }

}
