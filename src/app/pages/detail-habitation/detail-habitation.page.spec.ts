import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailHabitationPage } from './detail-habitation.page';

describe('DetailHabitationPage', () => {
  let component: DetailHabitationPage;
  let fixture: ComponentFixture<DetailHabitationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailHabitationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailHabitationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
